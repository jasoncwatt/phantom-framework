<?PHP
//Always show API responses reguardless of headers sent.
define('API', true);

require('config.php');
require('loader.php');

$checkSigned = new SignedAPI();
if($checkSigned->isAPI==true){
	$session = $checkSigned->checkData();
}else{
	$session = new Session();
}
//$session->destroy(); // yes, full destroy

// Run the router
$router->execute();