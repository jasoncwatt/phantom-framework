<?PHP

class SignedAPI {
	
	public $isAPI = false;
	public $authentic = false;
	private $sig;
	private $request;
	
	public function __construct(){
		if(isset($_SERVER['HTTP_COASTER-JUNCTION-SIG'])&&(isset($_GET['userid'])||isset($_POST['userid']))){
			$this->isAPI=true;
			$this->sig = $_SERVER['HTTP_COASTER-JUNCTION-SIG'];
			
			$rm = strtoupper($_SERVER['REQUEST_METHOD']);
			switch($rm){
				case 'POST': $this->request = $_POST;
				case 'GET': $this->request = $_GET;
			}
		}
	}
	
	public static function isAuthentic(){
		return $this->authentic;
	}
	
	public function checkData(){
		//$this->secret = SQL::getSecretKey($this->request['userid']);
		if(empty($this->secret)){ return NULL; }

		$key = $this->generateSignature($this->request,$secret);
		if($key==$this->sig){
			$this->authentic = true;
			//return SQL::getAccountLogin($this->request['userid']);
		}
	}
	
	private function generateSignature($data){
		//sort data array alphabetically by key
    ksort($data);
    //combine keys and values into one long string
    $dataString = '';
    foreach($data as $key => $value) {
        $dataString .= $key.$value;
    }
    //lowercase everything
    $dataString = strtolower($dataString);
    //generate signature using the SHA256 hashing algorithm
    return hash_hmac("sha1",$dataString,$this->secret);
	}
}

/**
* @param array $data Array of key/value pairs of data
* @param string $secretKey
* @return string A generated signature for the $data based on $secretKey

$USER_ID = "1234";
$SECRET_KEY = "bobs-super-secret-key";

function generateSignature($data,$secretKey)
{
		//sort data array alphabetically by key
    ksort($data);
    //combine keys and values into one long string
    $dataString = '';
    foreach($data as $key => $value) {
        $dataString .= $key.$value;
    }
    //lowercase everything
    $dataString = strtolower($dataString);
    //generate signature using the SHA256 hashing algorithm
    return hash_hmac("sha1",$dataString,$this->secret);
}
 
$bobsData = array(
    "userid" => $USER_ID,
    "email" => "newbob@example.com"
);
 
$sig = generateSignature($bobsData,$SECRET_KEY);
//add signature to the outgoing data
$bobsData['sig'] = $sig;
//generate HTTP query string
$queryString = http_build_query($bobsData);

header("coaster-junction-sig: ".$sig);

echo $queryString;

*/