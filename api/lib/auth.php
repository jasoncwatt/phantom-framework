<?PHP

class Auth {
	function __construct(){
		global $session;
		//setup
		$this->session = &$session;
	}
	
	function check($callback='',$denied_redirect=''){
		$group = 0;
		$checked=true;
		//DAO::checkLogin('','');
		if(isset($this->session['userid']) && isset($this->session['pagePermission'])){
			$checked = in_array(parent::callback,$this->session['pagePermission']);
		}else{
			//$checked = SQL::getPermission($group,parent::callback);
		}
		//pull config for the page that should be loaded.
		//check is user has permisison.
		//$checked = $this->sql->getPermission($group,$this->Callback);		
		
		if(!$checked && $callback!=$denied_redirect){
			return false;
		}
		return true;
	}
}