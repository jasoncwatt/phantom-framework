<?
class view {
	public $View = '404';
	public $Template='default';
	public $Data = [];
	public $Session;
	public $Meta;
	
	public function __construct(){
		global $session;
		
		$this->Meta = new stdClass();
		$this->Session = $session;
		$this->Meta->Title = TITLEDEFAULT;
	}
	
	public function render(){
		$this->ViewFile = VIEW.$this->View.'.php';
		$this->TemplateFile = TEMPLATE.$this->Template.'.php';
		
		if(!file_exists($this->ViewFile)){
			throw new APIException('The view '.$this->View.'.php file dose not exist');
		}
		if(!file_exists($this->TemplateFile)){
			throw new APIException('The template '.$this->Template.'.php file dose not exist');
		}
		$this->renderTemplate();
	}
	
	private function renderTemplate(){
		require($this->TemplateFile);
	}
	
	private function renderView(){
		require($this->ViewFile);
	}
	
	private function element($element=''){
		if(!file_exists(ELEMENT.$element.'.php')){
			throw new APIException('The element '.$element.'.php file dose not exist');
		}
		require(ELEMENT.$element.'.php');
	}
}