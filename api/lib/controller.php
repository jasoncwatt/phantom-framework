<?PHP
class controller {
	public $Session;
	private $AJAX=false;
	public $View;
	
	public function __construct(){
		global $session;
		require(LIB.'view.php');
		
		$this->Session = $session;
		$this->View = new View();
	}
	
	public function render(){
		if(AJAX){
			header('Content-Type: application/json');
			echo json_encode($this->View->Data);
			return;
		}
		$this->View->Render();
	}
}