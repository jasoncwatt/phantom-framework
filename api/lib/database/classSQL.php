<?php
class MySQL
{
	public $sqlList;
	public $selected;
	private $dbConfig;
	private $db;
	
	public function __construct($dbName='default') {
		include('dbconfig.php');
		if(isset($this->dbConfig[$dbName]) && is_array($this->dbConfig[$dbName])){
			$this->selected=$dbName;
			//$this->connect();
		}else{
			debug('error initilizing SQL');
		}
	}	
	public function getDatabase() {
	  	return $this->dbConfig[$this->selected]['database'];
	}
	private function connect(){
		$host = $this->dbConfig[$this->selected]['host'];
		$login = $this->dbConfig[$this->selected]['login'];
		$password = $this->dbConfig[$this->selected]['password'];
		$database = $this->dbConfig[$this->selected]['database'];
		try{
			$this->db = new PDO('mysql:host='.$host.';dbname='.$database.';charset=utf8', $login, $password);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		} catch (PDOException $e) {
			die('Could not connect to DB:'.$e);
		}
	}
	public function query($sql,$params=[])
  {
			$r = $this->m_q($sql,$params);
			return $r->fetchAll(PDO::FETCH_ASSOC);
  }
	public function select($a)
  {
		// Array Needed:
		// $a['table'] = table name
		// $a['fields'] = array('value')
		// $a['where'] = array('key'=>'value')
		// $a['limit'] = number
		// $a['order'] = array('key'=>'value')
		$fields = $limit = $where = $order = '';
		$params = [];
		if(empty($a['fields'])){
			$fields = '*';
		}else{
			foreach($a['fields'] as $k => $v)
			{
				$fields .= '`'.$v.'`, ';
			}
			$fields = substr($fields,0,-2);
		}
		if(!empty($a['order'])){
			$order = 'ORDER BY ';
			foreach($a['order'] as $k => $v){
				$order .= $k.' '.v.', ';
			}
			$order = substr($order,0,-2);
		}
		if(!empty($a['limit'])){
			$limit = ' LIMIT '.$a['limit'];
		}
		if(!empty($a['where'])){
			$where = $this->where($a['where'],$params);
		}
		return $this->m_q('SELECT '.$fields.' FROM '.mysql_real_escape_string($a['table']).' '.$where.' '.$order.$limit,$params); 
  }
	
	public function insert($a)
  {
		// Array Needed:
		// $a['table'] = table name
		// $a['values'] = array('key'=>'value')
		$keys =	$vals = '';
		$params = [];
		foreach($a['values'] as $k => $v)
		{
			$va = ':'.str_replace(' ','_',$k);
			$keys .= '`'.$k.'`, ';
			$vals .= $va.', ';
			$params[$va] = $v;
		}
		$this->m_q('INSERT INTO '.mysql_real_escape_string($a['table']).' ('.substr($keys,0,-2).') VALUES ('.substr($vals,0,-2).')',$params);
		return $this->db->lastInsertId();
  }
	
	public function update($a)
  {
		// Array Needed:
		// $a['table'] = table name
		// $a['values'] = array('key'=>'value')
		// $a['where'] = array('key'=>'value')
		$keys = $vals = $where = '';
		$params = [];
		foreach($a['values'] as $k => $v)
		{
			$va = ':v_'.$k;
			$params[$va] = $v;
			$keys .= "`".$k."`=".$va.", ";
		}
		$where = $this->where($a['where'],$params);
		if($where==false){
			return false;
		}
		return $this->m_q("UPDATE ".mysql_real_escape_string($a['table'])." SET ".substr($keys,0,-2)." WHERE ".$where."");
  }
	public function delete($a)
	{
		// Array Needed;
		// $a['table'] = table name
		// $a['where'] = array('key'=>'value')
		$params=[];
		$where = $this->where($a['where'],$params);
		if($where==false){
			return false;
		}
    return $this->m_q("DELETE FROM `".mysql_real_escape_string($a['table'])."` WHERE ".$where." LIMIT 1");
  }
	private function where($where, &$params){
		if(!isset($where)){ return false;}
		if(is_array($where)){
			foreach($where as $k => $v)
			{
				if(substr($v,0,1)=='"'||substr($v,0,1)=="'"||substr($v,0,2)=='fn'){
					$s = $v;
				}else{
					$va = ':w_'.$k;
					$params[$va] = $v;
					$s = $va;
				}				
				$where .= " `".$k."`=".$s." AND";
			}
			$where = substr($where,0,-4);
		}else{
			$where = $where;
		}
		return $where;
	}
	private function m_q($sql,$params)
	{
		try{
			$stmt = $this->db->prepare($sql);	
			$stmt->execute($params);
			$this->sqlList[]=$stmt->queryString;
			return $stmt->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			dbug($e);
			$this->dbug("MYSQL ERROR - Query:".$stmt->queryString.' | '.$e);
			$this->sqlList[]='ERROR: '.$stmt->queryString.' | '.$e;
			return false;
		}
	}
	private function dbug()
	{
		if (!isset($doc_root)) {
			$doc_root = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
		}
		$back =debug_backtrace();
		// you may want not to htmlspecialchars here
		$line = htmlspecialchars($back[0]['line']);
		$file = htmlspecialchars(str_replace(array('\\', $doc_root), array('/', ''), $back[0]['file']));
		
		$k =($back[1]['class'] == 'SQL')?3:1;
		
		$class = !empty($back[$k]['class']) ? htmlspecialchars($back[$k]['class']) . '::' : '';
		$function = !empty($back[$k]['function']) ? htmlspecialchars($back[$k]['function']) . '() ' : '';
		$args = (count($back[0]['args']>0))?$back[0]['args']:$back[0]['object'];
		$args = (count($args)==1 && $args[0]!='')?$args[0]:$args;

		print '<div style="background-color:#eee; width:100%; font-size:11px; font-family: Courier, monospace;" class="myDebug"><div style=" font-size:12px; padding:3px; background-color:#ccc">';
		print "<b>$class$function =&gt;$file #$line</b></div>";
		print '<div style="padding:5px;">';
		if(is_array($args) || is_object($args))
		{
			print '<pre>';
			print_r($args);
			print '</pre></div>';
		}else{
			print $args.'</div>';
		}
		print '</div>';
	}
}