<?php
require(LIB.'ig_route.php');
require(LIB.'controller.php');

$router = new Router;

$router->route( '/', 'index' );

$router->route( '/login', 'login' );
$router->route( '/logout', 'logout' );

// Specifying a default callback function if no other route is matched
$router->default_route( 'error404' );

// Specifying callback function permissions is denied (auth failure)
$router->denied_route( 'permDenied' );
$router->denied_redirect( 'login' );

